import { ProductService } from './../../services/product.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { Product } from './../../interfaces/product';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  private product: Product = {};
  private loading: any;
  private productId: string = null;
  private productSubscription: Subscription;

  constructor(private loadingCtrl: LoadingController, private toastCtrl: ToastController,
    private authService: AuthService, private activeRouter: ActivatedRoute, private productService: ProductService,
    private navCtrl: NavController) { 

      this.productId = this.activeRouter.snapshot.params['id'];

      if (this.productId) this.loadProduct();
    }

  ngOnInit() {
  }

  ngOnDestroy(){
    if(this.productSubscription) this.productSubscription.unsubscribe();
  }

  async saveProduct(){
    await this.presentLoading();

    this.product.userId = this.authService.getAuth().currentUser.uid;

    if (this.productId) {

      try {
        await this.productService.updateProduct(this.productId, this.product);
        await this.loading.dismiss();

        this.navCtrl.navigateBack('/home');

      } catch(error){
        this.presentToast('Error ao tentar salvar');
        this.loading.dismiss();
      }

    } else {
      this.product.createAt = new Date().getTime();

      try {
        await this.productService.addProducts(this.product);
        await this.loading.dismiss();

        this.navCtrl.navigateBack('/home');

      } catch(error){
        this.presentToast('Error ao tentar salvar');
        this.loading.dismiss();
      }

    }

  }

  loadProduct(){
    this.productSubscription = this.productService.getProduct(this.productId).subscribe(data => {
      this.product = data;
    });
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({
     message: 'Por favor, aguarde...'
   });
   return this.loading.present();
 }

 async presentToast(message: string) {
   const toast = await this.toastCtrl.create({
     message,
     duration: 2000
   });
   toast.present();
 }

}
