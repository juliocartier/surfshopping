import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afa: AngularFireAuth) { }
  //Funcao para realizar o login
  login (user: User){
   return this.afa.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  //Funcao para criar um login
  register(user: User) {
   return this.afa.auth.createUserWithEmailAndPassword(user.email, user.password);
  }
  //Funcao para sair da tela inicial
  logout() {
    return this.afa.auth.signOut();
  }
  //Funcao para autenticar
  getAuth(){
    return this.afa.auth;
  }
}
