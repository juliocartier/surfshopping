import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Product } from '../interfaces/product';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productsCollection: AngularFirestoreCollection<Product>;

  constructor(private afs: AngularFirestore) { 
    this.productsCollection = this.afs.collection<Product>('Products');
  }

  //Função para pegar todas as informações do documento do firebase
  getProducts(){
    return this.productsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data};
        });
      })
    );
  }

  addProducts(product: Product){
    return this.productsCollection.add(product);
  }

  getProduct(id: string){
    return this.productsCollection.doc<Product>(id).valueChanges();
  }

  updateProduct(id: string, product: Product){
/** 
 * Caso queira atualizar só um produto, é só usar esse método
 * return this.productsCollection.doc<Product>(id).set({price: '23457'});
*/

    return this.productsCollection.doc<Product>(id).update(product);
  }

  deleteProduct(id: string){
    return this.productsCollection.doc(id).delete();
  }
}
