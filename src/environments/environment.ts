// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCTXY4OPvncAMR5htOxR6ZWjY63SveqrQE",
    authDomain: "surfshop-1af76.firebaseapp.com",
    databaseURL: "https://surfshop-1af76.firebaseio.com",
    projectId: "surfshop-1af76",
    storageBucket: "surfshop-1af76.appspot.com",
    messagingSenderId: "718536585225",
    appId: "1:718536585225:web:399058e92685f5666448d8",
    measurementId: "G-7Q0J87QMHY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
